[[!meta title="PIL and Cairographics / PyCairo"]]

Please note that the code below was to only blur an cairo image surface and thus ignores conversion from BGRA to RGBA to BGRA that is needed in other situations. Theoretically just replace RGBA with BGRA in frombuffer and tostring.

    import cairo, Image, array

    im = cairo.ImageSurface.create_from_png("img11.png")

    im1 = Image.frombuffer("RGBA",( im.get_width(),im.get_height() ),im.get_data(),"raw","RGBA",0,1)

    im1 = im1.filter(ImageFilter.BLUR)
    im1 = im1.filter(ImageFilter.BLUR)
    im1 = im1.filter(ImageFilter.SMOOTH_MORE)

    #imgd = im1.tostring("raw","RGBA",0,1)
    imgd = im1.tostring()
    a = array.array('B',imgd)

    stride = self.width * 4
    surface = cairo.ImageSurface.create_for_data (a, cairo.FORMAT_ARGB32,
                                                  self.width, self.height, stride)
