[[!meta title="Download"]]

# Stable Code

## Releases

Please download one of the latest [releases](/releases) in order to
get an API-stable version of cairo. You'll need both the cairo and
pixman packages.

See In-Progress Development ([below](#development)) for details on
getting and building the latest pre-release source code if that's what
you're looking for.

## Binary Packages

### GNU/Linux distributions

Many distributions including [Debian](http://www.debian.org),
[Fedora](http://fedoraproject.org), and others regularly include
recent versions of cairo. As more and more applications depend on
cairo, you might find that the library is already installed. To get
the header files installed as well may require asking for a -dev or
-devel package as follows:

For Debian and Debian derivatives including Ubuntu:

	sudo apt-get install libcairo2-dev

For Fedora:

	sudo yum install cairo-devel

For openSUSE:

	zypper install cairo-devel

### Windows

Precompiled binaries for Windows platforms can be obtained in a
variety of ways.

From Dominic Lachowicz:

Since GTK+ 2.8 and newer depends on Cairo, you can have Cairo
installed on Win32 as a side-effect of installing GTK+. For example,
see [The Glade/GTK+ for Windows
Toolkit](http://gladewin32.sourceforge.net/).

From Daniel Keep (edited by Kalle Vahlman):

Go to [official GTK+ for Windows page](http://www.gtk.org/download/).

You probably want at least the zlib, cairo, and libpng run-time archives
(you can search on those strings to find them in the
page). That _should_ be it. Just pop libcairo-2.dll, libpng13.dll and
zlib1.dll into your working directory or system PATH, and away you go!

That gives you the base cairo functions, the PNG functions, and the
Win32 functions.

[[Building]]

### Mac OS X

Using [MacPorts](http://www.macports.org/), the port is called
'cairo', so you can just type:

       sudo port install cairo

And to upgrade to newer versions once installed:

       sudo port upgrade cairo

If you use [fink](http://www.finkproject.org/) instead, the command to
install cairo is:

       sudo apt-get install cairo

In general, fink is more conservative about upgrading packages than
MacPorts, so the MacPorts version will be closer to the bleeding edge,
while the fink version may well be more stable.

<h1 id="development">In-Progress Development</h1>

If you want to stay on the absolute cutting-edge of what's happening
with cairo, and you don't mind playing with software that is unstable
and full of rough edges, then we have several things you might enjoy:

## Snapshots

We may from time to time create a [snapshot](/snapshots) of the
current state of cairo. These snapshots do not guarantee API stability
as the code is still in an experimental state. Again, you'll want both
cairo and pixman packages from that directory.

## Browsing the latest code

The cairo library itself is maintained with the
[git](http://git-scm.com) version control system.  You may browse the
source online using the [cgit
interface](http://cgit.freedesktop.org/cairo/).

<h2 id="git"> Downloading the source with git </h2>

You may also use git to clone a local copy of the cairo library source
code. The following git commands are provided for your cut-and-paste
convenience.

	git clone git://anongit.freedesktop.org/git/cairo
	git clone git://anongit.freedesktop.org/git/pixman.git
	git clone git@gitlab.com:cairo/cairo-demos.git

followed by periodic updates in each resulting directory:

	git pull

Once you have a clone this way, you can browse it locally with
graphical tools such as gitk or gitview. You may also commit changes
locally with "git commit -a". These local commits will be
automatically merged with upstream changes when you next "git pull",
and you can also generate patches from them for submitting to the
cairo mailing list with "git format-patch origin". To compile the
clone, you need to run ./autogen.sh initially and then follow the
instructions in the file named INSTALL.

You may need some distribution-specific development packages to compile
cairo. If your are using Debian or Ubuntu, you may find
[[additional_details_for_Debian-derived_systems|AdditionalDetailsForDebianDerivedSystems]]
helpful.

### Git under Linux/UNIX

If you can't find git packages for your distribution, (though
check for a git-core package as well), you can get tar files from
<http://code.google.com/p/git-core/downloads/list>

### Git on Windows

You can use Git on Windows either with msysgit or git inside Cygwin. Msysgit is the recommended way to go as the installation is much simpler and it provides a GUI. In all cases, make sure the drive you download the repository on is formatted NTFS, as Git will generate errors on FAT32.

MSYSGIT

See the [msysgit project](http://code.google.com/p/msysgit/) for the
latest information about git for Windows. You'll find a .exe installer
for git there, as well as pointers to the cygwin port of git as well. This is all you need to do.

GIT ON CYGWIN

Download and run the Cygwin Setup.exe from Cygwin's website.  Walk
through the initial dialog boxes until you reach the "Select Packages"
page.  Click the "View" button to display an alphabetical list of
packages and select the 'git' package.

If you want to build using gcc, you will also need to select the
following packages:

* 'automake' ( anything >=1.7 )
* 'gcc-core'
* 'git' ( no need to click on this again if you did so above )
* 'gtkdoc'
* 'libtool'
* 'pkg-config'

Whether you build with gcc or not:

Click 'Next' and Cygwin setup will download all you need.  After
downloading, go to the directory where Cygwin is installed, and run
"cygwin.bat". This will open a command prompt.
[Mount](http://www.cygwin.com/cygwin-ug-net/using-utils.html#mount) an
existing directory on your hard drive, cd to that directory, and then
follow the "Downloading with git" instructions [above](#git).

If you want to build using Visual Studio and still want the latest
source, you *will* need cygwin, but you will only need to select the
'git' package.  See the [Visual
Studio](http://cairographics.org/visualstudio/) page for more details.

# Building On Windows (Mozilla Build Environment flavor)

<ol>
<li>Download and install zlib. Build zlib from its /projects folder, and use LIB RELEASE configuration.</li>
<li>Download and install libpng. Build libpng from its /projects folder and use LIB RELEASE configuration.</li>
<li>Install the MozillaBuild environment from here: <a href="http://developer.mozilla.org/en/docs/Windows_Build_Prerequisites#MozillaBuild">MozillaBuild</a></li>
<li>If you chose the default install path, you'll find in c:\mozilla-build several batch files. Edit the one corresponding to your version of VC++ (or a copy of it), and modify the INCLUDE and LIB paths. You'll need to add the zlib and libpng INCLUDE and LIBpaths. Also include &lt;your repository&gt;\cairo\src, &lt;your repository&gt;\cairo\boilerplate, and &lt;your repository&gt;\pixman\pixman to the INCLUDE paths. Here's an example (your paths will vary obviously):

<pre>
rem Root of cairo building
set "CAIROROOT=%HOMEDRIVE%%HOMEPATH%\BuildCairo"
rem Add zlib and libpng to LIB
set "LIB=%LIB%;%CAIROROOT%\zlib\projects\visualc6\Win32_LIB_Release;%CAIROROOT%\libpng\projects\visualc71\Win32_LIB_Release"
rem Add zlib and libpng to INCLUDE
set "INCLUDE=%INCLUDE%;%CAIROROOT%\zlib;%CAIROROOT%\libpng"
rem Add cairo, boilerplate, and pixman to INCLUDE
set "INCLUDE=%INCLUDE%;%CAIROROOT%\cairo\src;%CAIROROOT%\cairo\boilerplate;%CAIROROOT\pixman\pixman"</pre>

</li><li>Launch the batch file you just modified. It'll open a mingw window. Ignore the error messages that might appear at the top of the window.

We'll now build everything in debug configuration. For release, replace <i>CFG=debug</i> by <i>CFG=release</i></li>
<li>Browse to pixman's folder (pixman\pixman), and run make -f Makefile.win32 CFG=debug</li>
<li>Browse to Cairo's src folder (cairo\src), and run make -f Makefile.win32 static CFG=debug.

<b>You now have your library in cairo\src\debug (or release)</b>.

You can build the remaining ancillary cairo parts with the following steps, or go enjoy your library now.</li>
<li>Browse to Cairo's boilerplate folder (cairo\boilerplate), and run make -f Makefile.win32 CFG=debug</li>
<li>Browse to Cairo's test pdiff folder (cairo\test\pdiff), and run make -f Makefile.win32</li>
<li>Browse to Cairo's test folder (cairo\test), and run make -f Makefile.win32 CFG=debug</li>
<li>Browse to Cairo's benchmark folder (cairo\perf), and run make -f Makefile.win32 CFG=debug</li>
</ol>

## Building in Visual Studio

You can create a Visual Studio solution and projects for each of these: pixman, cairo/src, cairo/boilerplate, cairo/test, cairo/perf. Check each project's properties, make them all makefile projects, and set the build command (NMake) as follows. This example applies to cairo/src in release configuration, residing in C:\Work\Cairo\src, with the Mozilla Build Tools installed in C:\mozilla-build:

	C:
	cd Work\Cairo\src
	C:\mozilla-build\msys\local\bin\make.exe -f Makefile.win32 CFG=release

Modify the paths and the configuration as needed for the other projects.

