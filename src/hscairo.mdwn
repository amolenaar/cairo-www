[[!meta title="Haskell bindings"]]

The haskell bindings are available via darcs:

	darcs get http://ofb.net/~abe/darcs/cairo

There are also haskell bindings for libsvg-cairo, designed to work
well with hscairo:

	darcs get http://ofb.net/~abe/darcs/svg-cairo

-- Abe Egnor (1 Dec 2004)

# Updated Haskell bindings

There are also some new bindings based on Abe's work but updated to
the 0.9.x cairo API and made to work with the
[Gtk2Hs](http://haskell.org/gtk2hs/) Haskell bindings for Gtk+.

This project was supported by a Google Summer of Code grant. You can
read more about the development of these bindings in [Paolo's
blog](http://haskell.org/gtk2hs/archives/category/cairo/).

The current version of these bindings are built as part of Gtk2Hs but
they will eventually be released separately.

	darcs get --partial http://darcs.haskell.org/gtk2hs/

-- Duncan Coutts (11 Jan 2006)
