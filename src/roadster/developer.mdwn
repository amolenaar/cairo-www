[[!meta title="Roadster Developer Documentation"]]

Here there be monsters.  Only some of the internals are described below.

## Searching

All the search types (location, city, address) are implemented as independent
functions with signatures of the form:

	GList *search_function(const char *search_text)

They must return a list consisting of a struct search_result for each hit, and NULL
(an empty GList) if there are no hits.  The individual results look like:

	struct search_result {
		ESearchResultType type;
		char *text;
		glyph_t *glyph;
		mappoint_t *point;
		int zoom_level;
	};

The point and the text as well as the struct must be dynamically allocated,
and will be freed when the search is done.

In order to add a new search type, implement the search function and then add
it to the array of search functions in search.c, executed in order:

	static GList *(*search_functions[])(const char *) = {
		search_city_execute,
		search_location_execute,
		search_road_execute,
		search_coordinate_execute,
	};
