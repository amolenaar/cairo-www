<div class="tutright">[[!img "dash.png" link="no"]]</div>
	double dashes[] = {50.0,  /* ink */
	                   10.0,  /* skip */
	                   10.0,  /* ink */
	                   10.0   /* skip*/
	                  };
	int    ndash  = sizeof (dashes)/sizeof(dashes[0]);
	double offset = -50.0;
	
	cairo_set_dash (cr, dashes, ndash, offset);
	cairo_set_line_width (cr, 10.0);
	
	cairo_move_to (cr, 128.0, 25.6);
	cairo_line_to (cr, 230.4, 230.4);
	cairo_rel_line_to (cr, -102.4, 0.0);
	cairo_curve_to (cr, 51.2, 230.4, 51.2, 128.0, 128.0, 128.0);
	
	cairo_stroke (cr);
