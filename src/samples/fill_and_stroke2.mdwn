<div class="tutright">[[!img "fill_and_stroke2.png" link="no"]]</div>
	cairo_move_to (cr, 128.0, 25.6);
	cairo_line_to (cr, 230.4, 230.4);
	cairo_rel_line_to (cr, -102.4, 0.0);
	cairo_curve_to (cr, 51.2, 230.4, 51.2, 128.0, 128.0, 128.0);
	cairo_close_path (cr);
	
	cairo_move_to (cr, 64.0, 25.6);
	cairo_rel_line_to (cr, 51.2, 51.2);
	cairo_rel_line_to (cr, -51.2, 51.2);
	cairo_rel_line_to (cr, -51.2, -51.2);
	cairo_close_path (cr);
	
	cairo_set_line_width (cr, 10.0);
	cairo_set_source_rgb (cr, 0, 0, 1);
	cairo_fill_preserve (cr);
	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_stroke (cr);
